# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/simon/simon_bitbucket/opengl_tasks2019/external/GLM/glm/detail/glm.cpp" "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/external/GLM/glm/CMakeFiles/glm_static.dir/detail/glm.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLM_FORCE_DEPTH_ZERO_TO_ONE"
  "GLM_FORCE_PURE"
  "GLM_FORCE_RADIANS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../external/GLM"
  "../external/GLM/glm/.."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
