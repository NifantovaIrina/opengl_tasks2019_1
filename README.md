Репозиторий для заданий по компьютерной графике в МФТИ
======================================================

## Краткое руководство по встраиванию

* Создайте форк этого репозитория.
* Для каждого задания выделена отдельная папка (*task1*, *task2*).
* В папке с заданием создайте свою папку `<номер группы><фамилия на латинице>` (например, *123Ivanov*), работайте только в этой папке.
* Создайте вложенную подпапку `<номер группы><фамилия на латинице>Data<номер задания>` (например, *123IvanovData1*). Используйте эту папку для размещения загружаемых в программе файлов (3D модели, изображения и т.д.).
* В папке `<номер группы><фамилия на латинице>` создайте файл CMakeLists.txt следующего содержимого

```
set(SRC_FILES
    Main.h
    Main.cpp
)

MAKE_OPENGL_TASK(123Ivanov 1 "${SRC_FILES}")
# или MAKE_CUDA_TASK(123 Ivanov 1 "${SRC_FILES}")
```
    
Здесь в переменной **SRC_FILES** укажите имена ваших файлов с исходным кодом.
    
В аргументах макроса **MAKE_OPENGL_TASK** укажите имя папки и номер задания (1, 2 или 3).

