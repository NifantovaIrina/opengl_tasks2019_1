#pragma once


#include <fstream>
#include <memory>
#include <vector>

namespace mazekit {

class Maze;

typedef std::shared_ptr<Maze> MazePtr;
typedef std::vector<std::vector<bool> > BoolVec2D;

class Maze {
public:
	Maze(const std::string& filepath);
	~Maze() {};

	int width() const { return width_; };
	int height() const { return height_; };

	BoolVec2D mazeCells() const { return mazeCells_; };
	BoolVec2D verticalWalls() const { return verticalWalls_; };
	BoolVec2D horizontalWalls() const { return horizontalWalls_; };
	BoolVec2D angleWalls() const { return angleWalls_; };

private:
	void updateWalls();
	void updateVerticalWall(int y, int x);
	void updateHorizontalWall(int y, int x);
	void updateAngleWall(int y, int x);

	int width_;
	int height_;

	BoolVec2D mazeCells_;
	BoolVec2D verticalWalls_;
	BoolVec2D horizontalWalls_;
	BoolVec2D angleWalls_;
};

}